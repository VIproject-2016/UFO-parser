package ufoparser

import (
        "errors"
        "fmt"
        "github.com/PuerkitoBio/goquery"
        "github.com/juju/loggo"
        "os"
        "regexp"
        "time"
        "strings"
        "strconv"
)

const (
        NUFORC_URL = "http://www.nuforc.org/webreports"
        NUFO_DATETIME_LAYOUT = "1/2/06 15:04"
        NUFO_DATE_LAYOUT = "1/2/06"
)

var ZERO_DATE time.Time = NewYMDate(0, 1)
var INF_DATE time.Time = NewYMDate(4000, 12)
var NB_WORKERS int = 6
var LOG_LEVEL string = "ERROR"

/**
 * Informations about one monthly report summary page.
 * Example: http://www.nuforc.org/webreports/ndxe201608.html.
 */
type MonthlyReportPage struct {
        Href  string
        Count int
        Date  time.Time
}

/**
 * All the informations gathered on one UFO event.
 */
type RawUfoEvent struct {
        RawDate    string       `json:"rawDate",bson:"rawDate"`
        Date       time.Time    `json:"date",bson:"date"`
        Hours      bool         `json:"hours",bson:"hours"`
        City       string       `json:"city",bson:"city"`
        State      string       `json:"state",bson:"state"`
        Shape      string       `json:"shape",bson:"shape"`
        Duration   string       `json:"duration",bson:"duration"`
        Summary    string       `json:"summary",bson:"summary"`
        ReportLink string       `json:"reportLink",bson:"reportLink"`
        ReportText string       `json:"reportText",bson:"reportText"`
        Posted     time.Time    `json:"datePosted",bson:"datePosted"`
}

/**
 * This function will be called from a worker
 * goroutine each time a new ufo event has been found.
 */
type OnRawUfoEvent func(ufo RawUfoEvent)

/**
 * This struct define:
 * 1) a channel returning the UFO events as soon as they are
 * discovered.
 * 2) a boolean channel: true will be sent when the parse
 * function finishes
 *
 */
type RawUfoEventChan struct {
        Ufos chan RawUfoEvent
        Done chan bool
}

/**
 * A worker processes one job, which is:
 * 1) parse ufo event related information from one
 * row of the monthly report table page (the goquery selection is
 * a tr)
 * 2) fetch the full text report from the report event page
 */
type workerJob struct {
        s *goquery.Selection
        d *time.Time
}

/* logger. The level is set in the init function. */
var logger loggo.Logger
/* used to check that the monthly report url has a year/month
   information in the form: yyyymm */
var yymHtmlRegex *regexp.Regexp

/**
 * Create a RawUfoEventChan with a channel size of 1 (default).
*/
func NewRawUfoEventChan() *RawUfoEventChan {
        return &RawUfoEventChan{
                make(chan RawUfoEvent),
                make(chan bool),
        }
}

/**
 * Create a time.Time object specifying a year and month.
 * It eases the call to ParseRange.
 */
func NewYMDate(year int, month time.Month) time.Time {
        return NewDate(year, month, 0)
}

/**
 * Create a new date
*/
func NewDate(year int, month time.Month, day int) time.Time {
        return time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
}

// ----------------------------- init and utils

func init() {
        // configure logging
        logger = loggo.GetLogger("parser")
        loggo.ConfigureLoggers("parser=" + LOG_LEVEL)
        logger.Infof("Starting nuforc parser")

        var err error
        // initialise the regex
        yymHtmlRegex, err = regexp.Compile(`ndxe(?P<num>[0-9]{6}).html`)
        checkErr(err)
}

func checkErr(err error) {
        if err != nil {
                logger.Errorf("%s", err)
                os.Exit(1)
        }
}


// ------------------------------------------ parse first page (ndxevent.html)

/**
 * Parse the nuforc http://www.nuforc.org/webreports/ndxevent.html page to
 * extract links to monthly report pages.
 * dateFrom (exclusive) and dateTo (exclusive) are used to filter the pages returned.
 */
func LinksToMonthlyReports(dateFrom time.Time, dateTo time.Time) ([]MonthlyReportPage, error) {
        doc, err := goquery.NewDocument(NUFORC_URL + "/ndxevent.html")
        results := make([]MonthlyReportPage, 0)

        if err != nil {
                return results, err
        }
        doc.Find("tr").Each(func(i int, s *goquery.Selection) {
                pageInfo, err := parseIndexPageTd(s)
                if err == nil && pageInfo.Date.After(dateFrom) && pageInfo.Date.Before(dateTo) {
                        results = append(results, pageInfo)
                }
        })
        return results, nil
}

// parse one td from the main index page
func parseIndexPageTd(s *goquery.Selection) (page MonthlyReportPage, err error) {
        a := s.Find("td a")
        page.Href, _ = a.Attr("href")
        page.Count, err = strconv.Atoi(s.Find("td").Last().Text())
        matches := yymHtmlRegex.FindStringSubmatch(page.Href)
        if page.Href == "" || len(matches) == 0 {
                err = errors.New(fmt.Sprintf("could not find monthly report link: href=%s, yearMonth=%d", page.Href,
                        len(matches)))
                logger.Errorf("%s", err)
                return
        }
        page.Href = NUFORC_URL + "/" + page.Href
        page.Date, err = time.Parse("200601", matches[1])
        if err != nil {
                logger.Errorf("%s", err)
        }
        return
}

// extract all the trs from the monthly report page and send them to the worker channel
func extractTrsFromMonthlyReportPage(pageInfo MonthlyReportPage, jobQueue chan *workerJob) (nbTrs int, err error) {
        doc, err := goquery.NewDocument(pageInfo.Href)
        if err != nil {
                return
        } else {
                doc.Find("tbody tr").Each(func(i int, tr *goquery.Selection) {
                        jobQueue <- &workerJob{tr, &pageInfo.Date}
                        nbTrs++
                })
        }
        return
}

// parse one tr from the monthly report page and extract all the information
// about one ufo event.
func getUfoEvent(context *workerJob) (ufo RawUfoEvent, err error) {
        var date time.Time
        // check number of tds
        tds := context.s.Find("td")
        if tds.Length() < 7 {
                err = errors.New("getReport - not enough tds")
                return
        }
        // get date
        td := tds.First()
        ufo.RawDate = td.Text()
        date, ufo.Hours, err = parseEventDate(ufo.RawDate)
        if err != nil {
                logger.Errorf("month %v : could not parse date: %s\n", context.d, err)
                return
        }
        ufo.Date = correctYearOfDate(&date, context.d)
        // get link
        url, ok := td.Find("a").Attr("href")
        if !ok {
                logger.Errorf("%v : link not found\n", context)
                return ufo, errors.New("link not found")
        }
        ufo.ReportLink = NUFORC_URL + "/" + url;

        // get city
        td = td.Next()
        ufo.City = formatString(td.Text())
        // get state
        td = td.Next()
        ufo.State = formatString(td.Text())
        // get shape
        td = td.Next()
        ufo.Shape = formatString(td.Text())
        // get duration
        td = td.Next()
        ufo.Duration = td.Text()
        // get summary
        td = td.Next()
        ufo.Summary = td.Text()
        // parse date posted
        td = td.Next()
        d := td.Text()
        if date, err = time.Parse(NUFO_DATE_LAYOUT, d); err != nil {
                return
        }
        ufo.Posted = correctYearOfDate(&date, context.d)
        // ok, the only thing left is to get the full report text
        ufo.ReportText, err = getReportText(ufo.ReportLink)
        return
}

// parse the event date, dealing with date or datetime.
// hour will tell you if a time was specified or not
func parseEventDate(orig string) (d time.Time, hour bool, err error) {
        orig = strings.TrimSpace(orig)
        hour = true
        if d, err = time.Parse(NUFO_DATETIME_LAYOUT, orig); err == nil {
                return
        }
        hour = false
        if d, err = time.Parse(NUFO_DATE_LAYOUT, orig); err == nil {
                return
        }
        return
}

// format a string: trim + lowercase
func formatString(s string) string {
        return strings.ToLower(strings.TrimSpace(s))
}

// correct the year of datetime, since in the monthly report page years have only
// two digits.
// the correctYear should be a date with the correct year set
func correctYearOfDate(datetime, correctYear *time.Time) time.Time {
        return time.Date(correctYear.Year(), correctYear.Month(), datetime.Day(), datetime.Hour(), datetime.Minute(),
                0, 0, time.UTC)
}

// fetches the full report page and return the report text.
func getReportText(url string) (text string, err error) {
        doc, err := goquery.NewDocument(url)
        if err != nil {
                return
        }
        text = doc.Find("tbody tr:last-child td").Text()
        return
}


// a worker gather all the information about ufo events from monthly report page's TDs.
// if callback is not null, it will be called on each new event.
// if results chan is not null, ufos will be sent through it.
// each worker will send a bool through the done channel on job completion
func worker(id int, callback OnRawUfoEvent, jobQueue chan *workerJob, results chan RawUfoEvent, done chan bool) {
        logger.Infof("worker %d: starting\n", id)
        for ; jobQueue != nil; {
                job, ok := <-jobQueue
                if !ok {
                        logger.Tracef("worker %d: ufoTrs closed\n", id)
                        // channel closed, mark as done
                        jobQueue = nil

                } else if job == nil {
                        // end of input
                        close(jobQueue)
                        jobQueue = nil
                } else {
                        ufo, err := getUfoEvent(job);
                        logger.Tracef("ufo processed %s", ufo.Date)
                        if err == nil {
                                if results != nil {
                                        logger.Tracef("ufo SENT %s", ufo.Date)
                                        results <- ufo
                                }
                                if callback != nil {
                                        callback(ufo)
                                }
                        } else {
                                logger.Errorf("%d: --- %s\n", id, err)
                        }
                }
        }
        logger.Infof("worker %d: done\n", id)
        done <- true
}

// ------------------------------------------

/**
 * Parse all the ufo events from the nuforc.org web reports.
 * callback will be called FROM A GOROUTINE each time a new event
 * has been found.
 */
func ParseWithCallback(callback OnRawUfoEvent) {
        parse(ZERO_DATE, INF_DATE, callback, nil)
}

/**
 * Parse ufo events between from and to (exclusive).
 * callback will be called FROM A GOROUTINE each time a new event
 * has been found.
 */
func ParseRangeWithCallback(from, to time.Time, callback OnRawUfoEvent) {
        parse(from, to, callback, nil)
}

/**
 * Parse all the ufo events from the nuforc.org web reports.
 * new ufo events will be sent through results.ufos.
 * true will be sent through the results.done channel upon job
 * completion
 */
func Parse(results *RawUfoEventChan) {
        parse(ZERO_DATE, INF_DATE, nil, results)
}

/**
 * Parse ufo events between from and to (exclusive)
 * new ufo events will be sent through results.ufos.
 * true will be sent through the results.done channel upon job
 * completion.
 */
func ParseRange(from, to time.Time, results *RawUfoEventChan) {
        parse(from, to, nil, results)
}


// private parse function: launch the workers and wait until they finish.
func parse(from, to time.Time, callback OnRawUfoEvent, out *RawUfoEventChan) (err error) {

        // get links to monthly report pages
        links, err := LinksToMonthlyReports(from, to)

        if err != nil || len(links) == 0 {
                logger.Errorf("no links to parse: %s ", err)
                if out != nil {
                        out.Done <- true
                }
                return
        }
        // create channels
        jobQeue := make(chan *workerJob, 200)
        done := make(chan bool)
        var results chan RawUfoEvent
        if out != nil {
                results = out.Ufos
        }

        // launch the workers
        for i := 0; i < NB_WORKERS; i++ {
                go worker(i, callback, jobQeue, results, done)
        }
        // feed the workers
        logger.Tracef("number of links %d ", len(links))
        for _, href := range links {
                logger.Tracef("parsing %v ", href)
                nbTrs, err := extractTrsFromMonthlyReportPage(href, jobQeue)
                if err != nil {
                        logger.Errorf("%v: %s", href, err)
                } else {
                        logger.Tracef("%v : %d trs", href, nbTrs)
                }
        }
        jobQeue <- nil

        // wait for the workers to finish
        for i := 0; i < NB_WORKERS; i++ {
                <-done
                logger.Tracef("one worker done %d.", i)
        }

        // notify the caller that the job is done.
        if out != nil {
                logger.Infof("parse() finished.")
                out.Done <- true
        }

        return
}
