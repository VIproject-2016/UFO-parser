package main

import (
        "fmt"
        "gitlab.com/VIproject-2016/UFO-parser/parser"
        "gopkg.in/mgo.v2"
        "time"
        "os"
        "gitlab.com/VIproject-2016/UFO-parser/geocode"
        "regexp"
        "googlemaps.github.io/maps"
        //"encoding/json"
        "github.com/kr/pretty"
        "encoding/json"
        "github.com/juju/loggo"
        "io/ioutil"
)

var LOG_LEVEL string = "DEBUG"
var logger loggo.Logger

type Geo struct {
        Address  string                  `json:"address"`
        Geometry maps.AddressGeometry    `json:"geometry"`
}

type UfoEvent struct {
        ID      string          `json:"_id" bson:"_id,omitempty"`
        ufoparser.RawUfoEvent   `bson:",inline"`
        GeoInfo *Geo            `json:"geoinfo"`
}

var regexCityID *regexp.Regexp

func (ufo *UfoEvent) SetId() {
        ufo.ID = fmt.Sprintf("%s.%s.%s", ufo.Date.Format("060201.1504"),
                regexCityID.ReplaceAllString(ufo.City, "_"),
                ufo.Shape)
}

func (ufo *UfoEvent) SetGeo(geoResults []maps.GeocodingResult) {
        geo := Geo{}
        if len(geoResults) > 0 {
                geo.Address = geoResults[0].FormattedAddress
                geo.Geometry = geoResults[0].Geometry
        }
        ufo.GeoInfo = &geo
}

func init() {
        var err error
        regexCityID, err = regexp.Compile("[^A-Za-z0-9]+")
        if err != nil {
                fmt.Println(err)
                os.Exit(1)
                pretty.Println()
        }
        // configure logging
        logger = loggo.GetLogger("main")
        loggo.ConfigureLoggers("main=" + LOG_LEVEL)

}

func getConfig(config string) (dialInfo mgo.DialInfo) {
        raw, err := ioutil.ReadFile(config)
        if err != nil {
                logger.Criticalf("fail parsing config: %s",err.Error())
                os.Exit(1)
        }
        if err = json.Unmarshal(raw, &dialInfo); err != nil {
                logger.Criticalf("fail parsing config: %s",err.Error())
                os.Exit(1)
        }

        return
}

func main() {
        insertMongo()
}

func geocodeTest() {
        var callback ufoparser.OnRawUfoEvent = func(rufo ufoparser.RawUfoEvent) {
                ufo := UfoEvent{"", rufo, nil}
                ufo.SetId()
                geoInfo, err := geocode.Lookup(ufo.City + " " + ufo.State)
                if err == nil {
                        ufo.SetGeo(geoInfo)
                        if bs, err := json.Marshal(ufo); err == nil {
                                fmt.Printf("%s,\n", string(bs))

                        }
                } else {
                        fmt.Errorf("Error getting geoInfo", err);
                }
        }

        ufoparser.ParseRangeWithCallback(ufoparser.ZERO_DATE, ufoparser.NewYMDate(2010, time.Month(1)), callback)
}

func insertMongo() {
        if len(os.Args) < 2 {
                fmt.Println("Missing argument: user pass")
                os.Exit(1)
        }
        user := os.Args[1]
        pass := os.Args[2]

        info := &mgo.DialInfo{
                Addrs:    []string{"scyllarus.protectator.ch:27017"},
                Timeout:  60 * time.Second,
                Database: "nuforc",
                Username: user,
                Password: pass,
        }
        //fmt.Println("connecting to ", connectionString)
        session, err := mgo.DialWithInfo(info)
        if err != nil {
                logger.Criticalf("session not created")
                panic(err)
        }
        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

        conn := session.DB("nuforc").C("events")

        var callback ufoparser.OnRawUfoEvent = func(rufo ufoparser.RawUfoEvent) {
                ufo := UfoEvent{"", rufo, nil}
                ufo.SetId()
                if cnt, _  := conn.FindId(ufo.ID).Count(); cnt > 0 {
                        logger.Debugf("Skipping %s...", ufo.ID)
                        return // don't do the geo lookup if record already exists
                }
                geoInfo, err := geocode.Lookup(ufo.City + " " + ufo.State)
                if err == nil {
                        ufo.SetGeo(geoInfo)
                }else{
                        logger.Errorf("Fetch GeoInfo failed: %s %s %s\n", err, ufo.Date, ufo.ReportLink)
                }
                err = conn.Insert(ufo)
                if err != nil {
                        logger.Errorf("Insert failed: %s %s %s\n", err, ufo.Date, ufo.ReportLink)
                }
        }

        //ufoparser.ParseRangeWithCallback(ufoparser.ZERO_DATE, ufoparser.NewYMDate(2014, time.Month(4)), callback)
        ufoparser.ParseRangeWithCallback(ufoparser.NewYMDate(2001, time.Month(1)),
                ufoparser.NewYMDate(2002, time.Month(1)), callback)

        fmt.Println("done")
}

