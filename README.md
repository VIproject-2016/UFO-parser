# get the count of records from the site

On the event by date page, use the snippet:

    function yearCount(year){
        var count = 0;
        $('tr').each(function(i, tr){
           var $td1 = $(this).find('td').eq(0);
           if( $td1.text().indexOf(year) > 0 ){
             var cnt = $(this).find('td').eq(1).text(); 
             count += ~~cnt;
           }
        });
        return count;
    }
    
    yearCount("2015")

don't forget to include jQuery in the page first.
# Geolocation

Using the google maps js api:

    curl https://maps.googleapis.com/maps/api/geocode/json\?address\=$req\&key\=AIzaSyAfNcGFxpgAe0tMDEHWlvDjlrKwfj4XtUQ
    
With req="city no special chars and spaces replaced by +". Example:

    req="rhein-main+air+base+west+germany"
    

TODO: those records have not been inserted to mongo:

    VIproject-2016/UFO-parser ‹master✗› » go run main.go ufo UFoundOne                                                                                                                                                              1 ↵
    10:00:40 ERROR parser parser.go:169 could not find monthly report link: href=, yearMonth=0
    10:00:40 ERROR parser parser.go:169 could not find monthly report link: href=ndxe.html, yearMonth=0
    10:02:33 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-11-07 17:49:00 +0000 UTC http://www.nuforc.org/webreports/131/S131088.html
    10:04:38 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-11-03 02:00:00 +0000 UTC http://www.nuforc.org/webreports/131/S131002.html
    10:05:07 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-10-31 11:40:00 +0000 UTC http://www.nuforc.org/webreports/130/S130937.html
    10:06:09 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-10-25 16:00:00 +0000 UTC http://www.nuforc.org/webreports/131/S131050.html
    10:09:52 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-10-07 00:00:00 +0000 UTC http://www.nuforc.org/webreports/130/S130523.html
    10:12:48 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-09-24 20:05:00 +0000 UTC http://www.nuforc.org/webreports/130/S130153.html
    10:15:39 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-09-13 03:00:00 +0000 UTC http://www.nuforc.org/webreports/130/S130197.html
    10:16:27 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-09-09 01:00:00 +0000 UTC http://www.nuforc.org/webreports/129/S129835.html
    10:16:40 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-09-08 01:00:00 +0000 UTC http://www.nuforc.org/webreports/129/S129796.html
    10:17:24 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-09-04 23:00:00 +0000 UTC http://www.nuforc.org/webreports/129/S129981.html
    10:19:42 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-08-24 20:00:00 +0000 UTC http://www.nuforc.org/webreports/129/S129512.html
    10:23:08 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-08-10 22:00:00 +0000 UTC http://www.nuforc.org/webreports/129/S129151.html
    10:23:56 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-08-07 02:30:00 +0000 UTC http://www.nuforc.org/webreports/129/S129083.html
    10:30:43 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-07-16 19:50:00 +0000 UTC http://www.nuforc.org/webreports/128/S128543.html
    10:31:54 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-07-09 22:30:00 +0000 UTC http://www.nuforc.org/webreports/128/S128424.html
    10:33:45 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-07-04 20:00:00 +0000 UTC http://www.nuforc.org/webreports/128/S128353.html
    10:36:11 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-06-26 22:00:00 +0000 UTC http://www.nuforc.org/webreports/128/S128534.html
    10:36:50 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-06-23 01:30:00 +0000 UTC http://www.nuforc.org/webreports/128/S128038.html
    10:37:07 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-06-20 23:30:00 +0000 UTC http://www.nuforc.org/webreports/128/S128001.html
    10:39:31 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-06-06 10:30:00 +0000 UTC http://www.nuforc.org/webreports/127/S127734.html
    10:41:06 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-05-27 22:16:00 +0000 UTC http://www.nuforc.org/webreports/127/S127534.html
    10:42:01 ERROR main main.go:125 Fetch GeoInfo failed: maps: address, components and LatLng are all missing 2016-05-19 11:30:00 +0000 UTC http://www.nuforc.org/webreports/127/S127415.html
    10:43:15 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-05-11 15:00:00 +0000 UTC http://www.nuforc.org/webreports/127/S127312.html
    10:43:31 ERROR main main.go:125 Fetch GeoInfo failed: maps: address, components and LatLng are all missing 2016-05-09 00:00:00 +0000 UTC http://www.nuforc.org/webreports/127/S127367.html
    10:46:02 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-04-22 18:30:00 +0000 UTC http://www.nuforc.org/webreports/127/S127046.html
    10:46:27 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-04-20 00:55:00 +0000 UTC http://www.nuforc.org/webreports/126/S126943.html
    10:47:23 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-04-14 19:30:00 +0000 UTC http://www.nuforc.org/webreports/126/S126853.html
    10:47:30 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-04-13 11:00:00 +0000 UTC http://www.nuforc.org/webreports/126/S126843.html
    10:50:21 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-03-22 00:00:00 +0000 UTC http://www.nuforc.org/webreports/126/S126507.html
    10:52:42 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-03-05 17:45:00 +0000 UTC http://www.nuforc.org/webreports/126/S126213.html
    10:52:57 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-03-03 20:00:00 +0000 UTC http://www.nuforc.org/webreports/126/S126187.html
    10:54:09 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-02-28 19:30:00 +0000 UTC http://www.nuforc.org/webreports/126/S126109.html
    10:59:49 ERROR main main.go:125 Fetch GeoInfo failed: maps: ZERO_RESULTS -  2016-01-26 12:40:00 +0000 UTC http://www.nuforc.org/webreports/125/S125461.html
