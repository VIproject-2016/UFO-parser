package geocode

import (
        "log"
        "googlemaps.github.io/maps"
        "golang.org/x/net/context"
        "regexp"
        "strings"
)


var client *maps.Client
var regexNonAZ *regexp.Regexp


func init(){
        var err error
        client, err = maps.NewClient(maps.WithAPIKey("AIzaSyD-KNgTrVcJ2D7Wp_JOxVSRSiBigzml7sY"))
        if err != nil {
                log.Fatal(err)
        }
        regexNonAZ, err = regexp.Compile("[^A-Za-z0-9]+")
        if err != nil {
                log.Fatal(err)
        }
}


func EscapeAddress(s string) string {
        s = regexNonAZ.ReplaceAllString(s, "+")
        return strings.Trim(s, "+")
}

func Lookup(s string) ([]maps.GeocodingResult, error){
        // see https://godoc.org/googlemaps.github.io/maps#Client.Geocode
        r := &maps.GeocodingRequest{
                Address: EscapeAddress(s),
        }
        return client.Geocode(context.Background(), r)


        //fmt.Println("answer for " + s)
        //pretty.Println(resp)
}